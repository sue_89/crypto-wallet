package creative.com.cryptowallet.portfolio

import java.time.Instant
import java.time.ZoneOffset
import java.time.format.DateTimeFormatter

/**
 * PortfolioItems
 *
 * Collects all the necessary information for the Portfolio items
 */
class PortfolioItems {

    val amount: Float
    val price_usd: Float
    val tradedAt: String
    val symbolName: String
    val name: String


    constructor(symbolName: String, amount: Float, price: Float,name: String, tradeAt: String)  {
        this.symbolName = symbolName
        this.amount = amount
        this.price_usd = price
        this.tradedAt = tradeAt
        this.name = name
    }
}