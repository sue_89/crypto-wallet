package creative.com.cryptowallet.portfolio

import creative.com.cryptowallet.coindetails.CoinDetailsPresenter
import creative.com.cryptowallet.commons.AppSchedulerProvider
import creative.com.cryptowallet.commons.CryptoApi
import dagger.Module
import dagger.Provides

/**
 * UserPortfolioModule
 * Inject all the necessary dependencies for the UserPortfoloioActivity
 */
@Module
 class UserPortfolioModule {

    /**
     * Inject the presented
     */
 @Provides
 fun provideUserPortfolioPresenter(appScheduler: AppSchedulerProvider, cryptoApi: CryptoApi):
         UserPortfolioPresenter {
   return UserPortfolioPresenter(appScheduler, cryptoApi)
 }


}