package creative.com.cryptowallet.portfolio



/**
 * This class is the View Interface for the CryptoCurrenciesActivity.
 * It provides acces to the Activity from the presenter
 */
interface UserPortfolioView {

    /**
     * A new Currency item has been loaded
     * @param coinsItems A list of all the coins loaded
     */
    fun onCoinsItemLoaded(userCoinsItems: List<PortfolioItems>)


}