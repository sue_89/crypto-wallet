package creative.com.cryptowallet.portfolio

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import creative.com.cryptowallet.R
import creative.com.cryptowallet.model.CoinItem
import creative.com.cryptowallet.model.UserCoinItem

/**
 * UserPortfolioAdpater
 * This class is responsible for handleling the RecycleView Adapter.
 * It handles the different coins data for the user and display them
 */
class UserPortfolioAdpater : RecyclerView.Adapter<UserPortfolioAdpater.UserPortfolioViewHolder>() {

    private var mDataSource: List<PortfolioItems>? = null

    /**
     * Add new coinst to the data source
     * @param dataSource icons to add to the Adpater
     * @param concatenate true, if icons should be added. False, if they should be overwritten
     */
    fun setDataSource(dataSource: List<PortfolioItems>) {
        this.mDataSource = dataSource
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserPortfolioViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.view_user_item, parent, false)

        return UserPortfolioViewHolder(view)
    }



    override fun onBindViewHolder(holder: UserPortfolioViewHolder, position: Int) {
        val coinsItem = mDataSource!![position]
        holder.bind(coinsItem)

    }


    override fun getItemCount(): Int {
        return mDataSource?.size ?: 0
    }

    /**
     * CryptoCurrenciesViewHolder
     * This class handle each item of the list individually
     */
    class UserPortfolioViewHolder internal constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var portfolioItem: PortfolioItems? = null

        private val mName: TextView by lazy {
            itemView.findViewById<TextView>(R.id.coinName)
        }

        private val mCoinSymbol: TextView by lazy {
            itemView.findViewById<TextView>(R.id.coinSymbol)
        }

        private val mPriceUsd: TextView by lazy {
            itemView.findViewById<TextView>(R.id.priceUsd)
        }

        private val amount: TextView by lazy {
            itemView.findViewById<TextView>(R.id.amount)
        }

        private val tradeAT: TextView by lazy {
            itemView.findViewById<TextView>(R.id.tradeAt)
        }

        fun bind(portfolioItem: PortfolioItems) {
            this.portfolioItem = portfolioItem
            mName.text = portfolioItem.name
            mCoinSymbol.text = portfolioItem.symbolName
            mPriceUsd.text = portfolioItem.price_usd.toString()
            amount.text = portfolioItem.amount.toString()
            tradeAT.text = portfolioItem.tradedAt

        }

    }

}