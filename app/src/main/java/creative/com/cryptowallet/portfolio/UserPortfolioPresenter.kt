package creative.com.cryptowallet.portfolio

import creative.com.cryptowallet.coindetails.CoinDetailsView
import creative.com.cryptowallet.commons.CryptoApi
import creative.com.cryptowallet.commons.SchedulerProvider
import creative.com.cryptowallet.model.CoinItem
import creative.com.cryptowallet.model.HistoricalItem
import creative.com.cryptowallet.model.UserCoinItem
import io.realm.Realm
import rx.Observable
import rx.Subscription

/**
 * CoinDetailsPresenter
 * This class is the presenter for the CoinDetail Activity.
 * It makes all the background tasks for the activity
 */
class UserPortfolioPresenter {



    var mScheduleProvider: SchedulerProvider

    var mCryptoApi: CryptoApi
    private var mPortfolioView: UserPortfolioView? = null

    constructor(schedulerProvider: SchedulerProvider, cryptoApi: CryptoApi) {
        mScheduleProvider = schedulerProvider
        mCryptoApi = cryptoApi
    }


    /**
     * Called every time the activity is created.
     * It gets the historical data from the database or the server depending if it is the first time or not
     * @param view Interface for the activity
     */
    fun onViewCreated(view: UserPortfolioView) {
        mPortfolioView = view
        // Get the user portfolio from the database
        // TODO: This should be done from the server
        val realm = Realm.getDefaultInstance()
        val items = realm.where(UserCoinItem::class.java).findAll()
        var portfolioItems: ArrayList<PortfolioItems> = ArrayList()
        items.forEach {
            val coin = realm.where(CoinItem::class.java).equalTo("id", it.id).findFirst()
            portfolioItems.add(PortfolioItems(coin.symbol!!, it.amount!!, it.priceUsd!!,
                    coin.name!!, it.traceAt!!))


        }
        mPortfolioView!!.onCoinsItemLoaded(portfolioItems)

    }

}