package creative.com.cryptowallet.portfolio

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ProgressBar
import android.widget.TextView
import creative.com.cryptowallet.R
import dagger.android.AndroidInjection

import kotlinx.android.synthetic.main.activity_user_portfolio.*
import javax.inject.Inject


/**
 * UserPortfolioActivity
 * It retrieves all the information from the user
 */
class UserPortfolioActivity : AppCompatActivity(), UserPortfolioView {

    @Inject
    lateinit var userPortfolioPresenter: UserPortfolioPresenter

    private val mUserPortfolioAdapter = UserPortfolioAdpater()

    private val layoutManager = LinearLayoutManager(this)

    private val mRecyclerView: RecyclerView by lazy {
        findViewById<RecyclerView>(R.id.recycler_view) as RecyclerView
    }

    private val mProgressBar: ProgressBar by lazy {
        findViewById<ProgressBar>(R.id.progress_bar)
    }

    private val mStatusTextView: TextView by lazy {
        findViewById<TextView>(R.id.status_text_view)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_portfolio)
        setSupportActionBar(toolbar)
        prepareRecyclerView()
        mRecyclerView.adapter = mUserPortfolioAdapter
        userPortfolioPresenter.onViewCreated(this)

    }

    private fun prepareRecyclerView() {
        mRecyclerView.layoutManager = layoutManager
    }



    override fun onCoinsItemLoaded(userCoinsItems: List<PortfolioItems>) {
        mProgressBar.visibility = View.GONE
        if(userCoinsItems.isEmpty()) {
            mStatusTextView.setText(R.string.no_internet_connection)
            return
        }
        mStatusTextView.text = null
        mUserPortfolioAdapter.setDataSource(userCoinsItems)
    }


}
