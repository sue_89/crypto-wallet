package creative.com.cryptowallet.di

import android.app.Application
import android.content.Context
import creative.com.cryptowallet.commons.AppSchedulerProvider
import creative.com.cryptowallet.commons.BasicAuthInterceptor
import creative.com.cryptowallet.commons.Constants
import creative.com.cryptowallet.commons.CryptoApi
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import rx.schedulers.Schedulers
import javax.inject.Singleton
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor


/**
 * Created by ponssusa on 05/07/2018.
 */
@Module
class AppModule {

    @Provides
    @Singleton
    fun provideContext(application: Application): Context {
        return application
    }

    @Provides
    @Singleton
    fun provideAppSchedulerProvider(): AppSchedulerProvider {
        return AppSchedulerProvider()
    }

    @Provides
    @Singleton
    fun provideNewApiInterface(appSchedulerProvider: AppSchedulerProvider): CryptoApi {


        val httpClient = OkHttpClient.Builder()

        httpClient.addInterceptor(BasicAuthInterceptor("richard@rich.com", "secret"))

        val retrofit = Retrofit.Builder()
                .baseUrl(Constants.CRYPTO_ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.createWithScheduler(appSchedulerProvider.io()))
                .build()
        return retrofit.create(CryptoApi::class.java)
    }


}