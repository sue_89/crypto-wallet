package creative.com.cryptowallet.di

import android.app.Application
import creative.com.cryptowallet.AppDelegate
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

/**
 * Created by ponssusa on 05/07/2018.
 */
@Singleton
@Component(modules =  arrayOf(AndroidInjectionModule::class, AppModule::class, ActivityBuilder::class))
interface AppComponent {

    fun inject(app: AppDelegate)

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application) : Builder

        fun build() : AppComponent
    }
}
