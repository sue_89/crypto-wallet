package creative.com.cryptowallet.di

import creative.com.cryptowallet.coindetails.CoinDetailsActivity
import creative.com.cryptowallet.coindetails.CoinsDetailsModule
import creative.com.cryptowallet.cryptocurrencies.CryptoCurrenciesActivity
import creative.com.cryptowallet.cryptocurrencies.CryptoCurrenciesModule
import creative.com.cryptowallet.portfolio.UserPortfolioActivity
import creative.com.cryptowallet.portfolio.UserPortfolioModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by ponssusa on 05/07/2018.
 */

@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = arrayOf(CryptoCurrenciesModule::class))
    abstract fun bindCryptoCurrenciesActivity(): CryptoCurrenciesActivity

    @ContributesAndroidInjector(modules = arrayOf(CoinsDetailsModule::class))
    abstract fun bindCoinDetailActivity(): CoinDetailsActivity

    @ContributesAndroidInjector(modules = arrayOf(UserPortfolioModule::class))
    abstract fun bindUserPortfolioActivity(): UserPortfolioActivity
}
