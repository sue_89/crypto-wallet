package creative.com.cryptowallet.coindetails

import creative.com.cryptowallet.commons.AppSchedulerProvider
import creative.com.cryptowallet.commons.CryptoApi
import dagger.Module
import dagger.Provides

/**
 * Created by ponssusa on 04/07/2018.
 */
@Module
 class CoinsDetailsModule {

 @Provides
 fun provideCoinDetailsPresenter(appScheduler: AppSchedulerProvider, cryptoApi: CryptoApi): CoinDetailsPresenter {
   return CoinDetailsPresenter(appScheduler, cryptoApi)
 }


}