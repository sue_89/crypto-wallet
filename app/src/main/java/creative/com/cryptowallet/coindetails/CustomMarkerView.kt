package creative.com.cryptowallet.coindetails

import android.content.Context
import com.github.mikephil.charting.components.MarkerView
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.highlight.Highlight
import creative.com.cryptowallet.model.HistoricalItem
import kotlinx.android.synthetic.main.marker_view.view.*

/**
 * CustomMarkerView
 * A custom implementation of the Marker View
 */
class CustomMarkerView(context: Context,
                       layoutResource: Int,
                       private val models: List<HistoricalItem>) : MarkerView(context, layoutResource) {

    // callbacks everytime the MarkerView is redrawn, can be used to update the
    // content (user-interface)
    override fun refreshContent(e: Entry, highlight: Highlight) {
        val xIndex = e.xIndex
        val model = models[xIndex]
        tvPrice.text = "Price: " + model.priceUsd
        tvDate.text = "Date: " + model.snapshotAtt
    }

    override fun getXOffset(f: Float): Int {
        // this will center the marker-view horizontally
        return -(width / 2)
    }

    override fun getYOffset(f: Float): Int {
        // this will cause the marker-view to be above the selected value
        return -height
    }

}