package creative.com.cryptowallet.coindetails

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import creative.com.cryptowallet.R
import creative.com.cryptowallet.commons.Constants
import creative.com.cryptowallet.model.CoinItem
import creative.com.cryptowallet.model.HistoricalItem
import dagger.android.AndroidInjection

import kotlinx.android.synthetic.main.activity_coin_details.*
import javax.inject.Inject
import android.support.design.widget.BottomSheetDialog
import android.widget.*
import creative.com.cryptowallet.portfolio.UserPortfolioActivity
import kotlinx.android.synthetic.main.dialog_add_coin.*


/**
 * CoinDetailsActivity
 * Activity responsible of showing the details of a Coin
 * Implemented the CoinsDetailsView
 */
class CoinDetailsActivity : AppCompatActivity(), CoinDetailsView {

    @Inject
    lateinit var coinDetailsPresenter: CoinDetailsPresenter

    private val mProgressBar: ProgressBar by lazy {
        findViewById<ProgressBar>(R.id.progress_bar)
    }

    private val mStatusTextView: TextView by lazy {
        findViewById<TextView>(R.id.status_text_view)
    }

    private val mName: TextView by lazy {
        findViewById<TextView>(R.id.coinName)
    }

    private val mCoinSymbol: TextView by lazy {
        findViewById<TextView>(R.id.coinSymbol)
    }

    private val mPriceUsd: TextView by lazy {
        findViewById<TextView>(R.id.priceUsd)
    }

    private val mHour: TextView by lazy {
        findViewById<TextView>(R.id.oneHour)
    }

    private val mWeek: TextView by lazy {
        findViewById<TextView>(R.id.sevenDay)
    }


    private var mAddCoinDialog: BottomSheetDialog? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_coin_details)
        setSupportActionBar(toolbar)
        val iconId:Int = intent.getIntExtra(Constants.ICON_ITEM_KEY, 0)
        coinDetailsPresenter.onViewCreated(this, iconId)

        fabAdd.setOnClickListener { _ ->
            val bottomSheetLayout = layoutInflater.inflate(R.layout.dialog_add_coin, null)
            bottomSheetLayout.findViewById<Button>(R.id.button_close).setOnClickListener(View.OnClickListener {
                mAddCoinDialog!!.dismiss()
            })
            bottomSheetLayout.findViewById<Button>(R.id.button_ok).setOnClickListener(View.OnClickListener {
                val amountTxt: EditText = bottomSheetLayout.findViewById<EditText>(R.id.amount)
                coinDetailsPresenter.addNewCoinToPortfolio(amountTxt.text.toString().toFloat())
                mAddCoinDialog!!.dismiss()
            })

            mAddCoinDialog = BottomSheetDialog(this)
            mAddCoinDialog!!.setContentView(bottomSheetLayout)
            mAddCoinDialog!!.show()
        }

    }

    override fun onStop() {
        super.onStop()
        coinDetailsPresenter.onDestroy()
    }

    override fun onShowProgress() {
        mProgressBar.visibility = View.VISIBLE
    }

    override fun onHideProgress() {
        mProgressBar.visibility = View.GONE
    }

    override fun onHistoricalBytesLoaded(historicalData: List<HistoricalItem>) {
        val thread = Thread(Runnable {
            runOnUiThread { renderLineChart(historicalData) }
        })
        thread.start()
    }

    override fun onCoinLoaded(coin: CoinItem) {
        mName.text = coin.name
        mPriceUsd.text = coin.priceUsd
        mHour.text = coin.changeHour
        mWeek.text = coin.changeWeek
        mCoinSymbol.text = coin.symbol
    }

    override fun onCoinAdded() {
        Toast.makeText(this, "Coin added successfully", Toast.LENGTH_SHORT).show();
    }

    /**
     * Given the historical data, displays it in a linear graph
     */
    private fun renderLineChart(historicalData: List<HistoricalItem>) {
        val entries = historicalData.mapIndexedTo(ArrayList<Entry>()) {
            i, model -> Entry(model.priceUsd!!.toFloat(), i)
        }
        val dataset = LineDataSet(entries, "")
        dataset.setDrawCubic(true)
        dataset.setDrawFilled(true)
        dataset.setDrawHighlightIndicators(true)
        //
        dataset.lineWidth = 1.95f
        dataset.circleRadius = 5f
        dataset.color = Color.parseColor("#EFEFFF")
        dataset.setDrawCircleHole(false)
        dataset.setDrawCircles(false)
        dataset.highLightColor = Color.WHITE
        dataset.setDrawValues(false)
        val labels =historicalData.map { it.snapshotAtt!!.substring(5,10 ) }
        val data = LineData(labels, dataset)
        linechart.setDescription("Price Chart")
        linechart.data = data
        linechart.xAxis.setDrawGridLines(false)
        linechart.axisLeft.setDrawGridLines(false)
        linechart.xAxis.axisLineColor = resources.getColor(R.color.colorPrimary)//top line
        linechart.xAxis.textColor = resources.getColor(R.color.colorPrimary)
        linechart.axisLeft.axisLineColor = resources.getColor(R.color.colorPrimary)//left line
        linechart.axisLeft.textColor = resources.getColor(R.color.colorPrimary)
        linechart.axisRight.axisLineColor = resources.getColor(R.color.colorPrimary)//right line
        linechart.axisRight.textColor = resources.getColor(R.color.colorPrimary)
        linechart.setDrawBorders(false)
        linechart.setDrawGridBackground(false)
        linechart.setDescriptionColor(Color.WHITE)
        linechart.isAutoScaleMinMaxEnabled = false


        //Custom marker
        linechart.setDrawMarkerViews(true)
        val markerView = CustomMarkerView(applicationContext, R.layout.marker_view, historicalData)
        linechart.markerView = markerView
        linechart.setTouchEnabled(true)



        // hide legend
        val legend = linechart.legend
        legend.isEnabled = false
        linechart.invalidate()
        linechart.animateXY(300, 300)
    }

    override fun onError() {
        mProgressBar.visibility = View.GONE
        mStatusTextView.setText(R.string.unkown_error)
    }
}

