package creative.com.cryptowallet.coindetails

import creative.com.cryptowallet.commons.CryptoApi
import creative.com.cryptowallet.commons.PortfolioRequest
import creative.com.cryptowallet.commons.SchedulerProvider
import creative.com.cryptowallet.model.CoinItem
import creative.com.cryptowallet.model.HistoricalItem
import io.realm.Realm
import rx.Observable
import rx.Subscription

/**
 * CoinDetailsPresenter
 * This class is the presenter for the CoinDetail Activity.
 * It makes all the background tasks for the activity
 */
class CoinDetailsPresenter {


    private var subscription: Subscription? = null

    var mScheduleProvider: SchedulerProvider

    var mCryptoApi: CryptoApi
    var mCoinId: Int? = null
    private var mCoinDetailsView: CoinDetailsView? = null

    constructor(schedulerProvider: SchedulerProvider, cryptoApi: CryptoApi) {
        mScheduleProvider = schedulerProvider
        mCryptoApi = cryptoApi
    }


    /**
     * Called every time the activity is created.
     * It gets the historical data from the database or the server depending if it is the first time or not
     * @param view Interface for the activity
     */
    fun onViewCreated(view: CoinDetailsView, coinId: Int) {
        mCoinDetailsView = view
        mCoinId = coinId
        loadHistoricalData()
        getCoinItem()


    }

    /**
     * Tris to get the historical data. If it is not possible, an error is shown
     */
    private fun loadHistoricalData () {
        mCoinDetailsView?.onShowProgress()
        subscription = mCryptoApi!!
                .getHistoricalData(mCoinId!!)
                .map { it.historicalList }
                .flatMap({ items ->
                    Realm.getDefaultInstance().executeTransaction({ realm ->
                        // Delete all items before adding new ones
                        val realItems = realm.where(HistoricalItem::class.java).findAll()
                        if (realItems != null) {
                            realItems.deleteAllFromRealm()
                        }
                        realm.insert(items)

                    })
                    Observable.just(items)
                })
                .observeOn(mScheduleProvider.ui())
                .doOnTerminate {
                    mCoinDetailsView?.onHideProgress()
                }
                .subscribe({
                    mCoinDetailsView?.onHistoricalBytesLoaded(it!!) },
                        { mCoinDetailsView?.onError() })

    }

    fun addNewCoinToPortfolio(amount: Float) {
        val realm = Realm.getDefaultInstance()
        val coinItem = realm.where(CoinItem::class.java).equalTo("id", mCoinId).findFirst()
        val portfolioRequest: PortfolioRequest = PortfolioRequest(mCoinId!!, amount,
                coinItem!!.priceUsd!!.toFloat(), null)
        subscription = mCryptoApi!!
                .postPortolio(portfolioRequest)
                .map { it.userCoin }
                .flatMap({ items ->
                    Realm.getDefaultInstance().executeTransaction({ realm ->
                        // Delete all items before adding new ones
                        realm.insertOrUpdate(items)

                    })
                    Observable.just(items)
                })
                .observeOn(mScheduleProvider.ui())
                .doOnTerminate {
                    mCoinDetailsView?.onHideProgress()
                }
                .subscribe({
                    mCoinDetailsView?.onCoinAdded() },
                        { mCoinDetailsView?.onError() })
    }

    /**
     * Get the Coin from the database
     */
    private fun getCoinItem() {
        val realm = Realm.getDefaultInstance()
        val coinItem = realm.where(CoinItem::class.java).equalTo("id", mCoinId).findFirst()
        mCoinDetailsView?.onCoinLoaded(coinItem)
    }




    /**
     * Unsubscribe from the asyncronous events
     */
    fun onDestroy() {
        subscription?.unsubscribe()
    }

}