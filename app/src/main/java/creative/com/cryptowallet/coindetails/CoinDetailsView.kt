package creative.com.cryptowallet.coindetails

import creative.com.cryptowallet.model.CoinItem
import creative.com.cryptowallet.model.HistoricalItem

/**
 * This class is the View Interface for the CryptoCurrenciesActivity.
 * It provides acces to the Activity from the presenter
 */
interface CoinDetailsView {

    fun onShowProgress()

    fun onHideProgress()

    fun onHistoricalBytesLoaded(historicalData: List<HistoricalItem>)

    fun onError()


    fun onCoinLoaded(coin: CoinItem)

    fun onCoinAdded()

}