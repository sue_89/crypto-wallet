package creative.com.cryptowallet.commons

import rx.Scheduler

/**
 * SchedulerProvider
 * This Interface provides an extraction layer for the Scheduler
 */
interface SchedulerProvider {

    fun ui(): Scheduler
    fun computation(): Scheduler
    fun trampoline(): Scheduler
    fun newThread(): Scheduler
    fun io(): Scheduler
}