package creative.com.cryptowallet.commons

import creative.com.cryptowallet.model.CoinsResponse
import creative.com.cryptowallet.model.HistoricalResponse
import creative.com.cryptowallet.model.PortfolioResponse
import retrofit2.http.*
import rx.Observable

/**
 * CryptoApi
 * Contains all the different requests for the Crypto API
 */
interface CryptoApi {

    /**
     * Get all the coins available
     * @param pageIndex Number of page to get the coins
     */
    @GET("/coins")
    fun getCoins(@Query("page") pageIndex: Int): Observable<CoinsResponse>


    /**
     * Get Historical Response for a given response
     * @param pageIndex Number of page to get the coins
     */
    @GET("/coins/{coin_id}/historical")
    fun getHistoricalData(@Path("coin_id") coinId: Int): Observable<HistoricalResponse>

    /**
     * Get Historical Response for a given response
     * @param pageIndex Number of page to get the coins
     */
    @POST("/portfolio")
    fun postPortolio(@Body body: PortfolioRequest): Observable<PortfolioResponse>
}