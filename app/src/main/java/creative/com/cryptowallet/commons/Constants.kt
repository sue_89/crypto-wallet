package creative.com.cryptowallet.commons

/**
 * Constants
 * It contains all the global variables for the application
 */

class Constants {

    companion object {

        // End point URL for the Crypto API
        val CRYPTO_ENDPOINT = "https://test.cryptojet.io"

        // Key for IconItem between activities
        val ICON_ITEM_KEY = "iconItem"

    }
}
