package creative.com.cryptowallet.commons

import java.time.*
import java.time.format.DateTimeFormatter
import java.time.format.DateTimeFormatter.ISO_LOCAL_DATE_TIME


/**
 * Created by ponssusa on 06/07/2018.
 */
class PortfolioRequest {

    val coin_id: Int
    val amount: Float
    val price_usd: Float
    val traded_at: String
    val notes: String?

    constructor(coinId: Int, amount: Float, price: Float, notes: String?)  {
        this.coin_id = coinId
        this.amount = amount
        this.price_usd = price
        val time: Long = System.currentTimeMillis()
        this.traded_at = ISO_LOCAL_DATE_TIME.format(Instant.ofEpochMilli(time).atZone(ZoneOffset.UTC))
        this.notes = notes
    }
}