package creative.com.cryptowallet

import android.app.Application
import android.app.Activity
import creative.com.cryptowallet.di.DaggerAppComponent
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import javax.inject.Inject
import io.realm.Realm
import io.realm.RealmConfiguration


/**
 * AppDelegate
 * This is the Applcaition class. Is responsible of injecting the different
 * activities and modules for the applciation
 */
class AppDelegate : Application() , HasActivityInjector {

    @Inject
    lateinit var activityDispatchingAndroidInjector: DispatchingAndroidInjector<Activity>



    override fun activityInjector(): DispatchingAndroidInjector<Activity>? {
        return activityDispatchingAndroidInjector
    }

    override fun onCreate() {
        super.onCreate()

        Realm.init(this)
        val config = RealmConfiguration.Builder().build()
        Realm.setDefaultConfiguration(config)

        DaggerAppComponent.builder()
                .application(this)
                .build()
                .inject(this)

    }

}