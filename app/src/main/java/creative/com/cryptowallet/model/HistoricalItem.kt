package creative.com.cryptowallet.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import io.realm.annotations.Required

/**
 * HistoricalItem
 * Ths class is a Realm Object containing all the necessary information for
 * the historical data of a coin
 */
open class HistoricalItem : RealmObject() {

    @SerializedName("price_usd")
    @Expose
    var priceUsd: String? = null

    @SerializedName("snapshot_at")
    @Expose
    var snapshotAtt: String? = null

}