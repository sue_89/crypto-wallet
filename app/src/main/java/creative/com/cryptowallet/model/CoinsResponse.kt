package creative.com.cryptowallet.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.ArrayList

/**
 * This model class represents the response from the server
 */

class CoinsResponse {

    @SerializedName("coins")
    @Expose
    var coins: CoinsPage? = null

}
