package creative.com.cryptowallet.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import io.realm.annotations.Required


/**
 * Ths class is a Realm Object containning all the necessary information for
 * a Crytpo Currency Item
 */
open class CoinItem : RealmObject() {

    @SerializedName("id")
    @Expose
    @PrimaryKey
    @Required
    var id: Int? = null

    @SerializedName("name")
    @Expose
    var name: String? = null

    @SerializedName("symbol")
    @Expose
    var symbol: String? = null

    @SerializedName("logo")
    @Expose
    var logo: String? = null

    @SerializedName("rank")
    @Expose
    var rank: Int? = null

    @SerializedName("price_usd")
    @Expose
    var priceUsd: String? = null

    @SerializedName("price_btc")
    @Expose
    var priceBtc: String? = null

    @SerializedName("24h_volume_usd")
    @Expose
    var VolumeUsd: Long? = null

    @SerializedName("market_cap_usd")
    @Expose
    var marketCapUsd: Long? = null

    @SerializedName("available_supply")
    @Expose
    var availableSupply: Long? = null

    @SerializedName("total_supply")
    @Expose
    var totalSupply: Long? = null

    @SerializedName("percent_change_1h")
    @Expose
    var changeHour: String? = null

    @SerializedName("percent_change_24h")
    @Expose
    var changeDay: String? = null

    @SerializedName("percent_change_7d")
    @Expose
    var changeWeek: String? = null

    @SerializedName("created_at")
    @Expose
    var createAt: String? = null

    @SerializedName("updated_at")
    @Expose
    var updatedAt: String? = null

}