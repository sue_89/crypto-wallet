package creative.com.cryptowallet.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.ArrayList

/**
 * Created by ponssusa on 04/07/2018.
 */
class CoinsPage {

    @SerializedName("total")
    @Expose
    var totalItems: Int? = null

    @SerializedName("per_page")
    @Expose
    var nItemPage: Int? = null

    @SerializedName("current_page")
    @Expose
    var currentPage: Int? = null

    @SerializedName("last_page")
    @Expose
    var lastPage: Int? = null

    @SerializedName("first_page_url")
    @Expose
    var firstPageUrl: String? = null

    @SerializedName("last_page_url")
    @Expose
    var lastPageUrl: String? = null

    @SerializedName("next_page_url")
    @Expose
    var nextPageUrl: String? = null

    @SerializedName("prev_page_url")
    @Expose
    var prevPageUrl: String? = null

    @SerializedName("path")
    @Expose
    var path: String? = null

    @SerializedName("from")
    @Expose
    var from: Int? = null

    @SerializedName("to")
    @Expose
    var to: Int? = null


    @SerializedName("data")
    @Expose
    var coinsItems: List<CoinItem> = ArrayList<CoinItem>()


}
