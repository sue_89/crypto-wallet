package creative.com.cryptowallet.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * This model class represents the response from the server for the historical data
 */

class HistoricalResponse {

    @SerializedName("historical")
    @Expose
    var historicalList: List<HistoricalItem>? = null

}