package creative.com.cryptowallet.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import io.realm.annotations.Required

/**
 * HistoricalItem
 * Ths class is a Realm Object containing all the necessary information for
 * the historical data of a coin
 */
open class UserCoinItem : RealmObject() {

    @SerializedName("coin_id")
    @Expose
    @PrimaryKey
    @Required
    var id:Int? = null

    @SerializedName("amount")
    @Expose
    var amount: Float? = null

    @SerializedName("price_usd")
    @Expose
    var priceUsd: Float? = null


    @SerializedName("traded_at")
    @Expose
    var traceAt: String? = null

}
