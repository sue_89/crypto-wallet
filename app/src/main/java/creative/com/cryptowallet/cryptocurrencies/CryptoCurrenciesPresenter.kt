package creative.com.cryptowallet.cryptocurrencies

import android.os.Bundle
import creative.com.cryptowallet.commons.CryptoApi
import creative.com.cryptowallet.commons.SchedulerProvider
import creative.com.cryptowallet.model.CoinItem
import io.realm.Realm
import rx.Observable
import rx.Subscription

/**
 * CryptoCurrenciesPresenter
 * This class is the presenter for the CryptoCurrencies Activity.
 * It makes all the background tasks for the activity
 */

class CryptoCurrenciesPresenter {

    var mCryptoApi: CryptoApi
    private var mCryptoCurrenciesView: CryptoCurrenciesView? = null

    private var subscription: Subscription? = null

    private var currentPage: Int = 1

    private var areMorePagesAvailable: Boolean = true

    private var isLoading: Boolean = false

    var mScheduleProvider: SchedulerProvider

    constructor(schedulerProvider: SchedulerProvider, cryptoApi: CryptoApi) {
        mScheduleProvider = schedulerProvider
        mCryptoApi = cryptoApi
    }


    /**
     * Called everytim the activity is created.
     * It get the item from the database or the server depending if it is the first time or not
     * @param view Interface for the activity
     */
    fun onViewCreated(view: CryptoCurrenciesView, savedInstanceState: Bundle?) {
        mCryptoCurrenciesView = view
        if (!isLoading && savedInstanceState != null) {
            // If the view have been already created just returns the items from the database
            val realm = Realm.getDefaultInstance()
            val items = realm.where(CoinItem::class.java).findAll()
            if (items == null) {
                mCryptoCurrenciesView?.onError()
            } else {
                mCryptoCurrenciesView?.onCoinsItemLoaded(items)
            }
            mCryptoCurrenciesView?.hideLoading()
        } else {
            currentPage = 1
            loadCoins()
        }

    }


    /**
     * It loads the coins for the first page
     */
    fun refreshCoins() {
        areMorePagesAvailable = true
        currentPage = 1
        loadCoins()
    }


    /**
     * Downloads the coins for the current page from the server.
     * If something goes wrong, it tries to return the Coins from the database
     */
    fun loadCoins() {
        if (!areMorePagesAvailable) {
            return
        }

        mCryptoCurrenciesView?.showLoading()
        isLoading = true
        subscription = mCryptoApi!!
                .getCoins(currentPage )
                .map { it.coins }
                .flatMap({ items ->
                    Realm.getDefaultInstance().executeTransaction({ realm ->
                        realm.insertOrUpdate(items?.coinsItems)
                        currentPage ++
                        if(items?.lastPage == items?.currentPage) {
                            areMorePagesAvailable = false
                        }
                    })
                    Observable.just(items!!.coinsItems)
                })
                .onErrorResumeNext { _ ->
                    val realm = Realm.getDefaultInstance()
                    val items = realm.where(CoinItem::class.java).findAll()
                    Observable.just(realm.copyFromRealm(items))
                }
                .observeOn(mScheduleProvider.ui())
                .doOnTerminate {
                    isLoading = false
                    mCryptoCurrenciesView?.hideLoading()
                }
                .subscribe({
                    mCryptoCurrenciesView?.onCoinsItemLoaded(it) },
                        { mCryptoCurrenciesView?.onError() })

    }

    /**
     * Check if the coins are getting download from the server or not
     * @return true if it is loading, false otherwise
     */
    fun areCoinsLoading(): Boolean {
        return isLoading
    }

    /**
     * Unsubscribe from the asyncronous events
     */
    fun onDestroy() {
        subscription?.unsubscribe()
    }

}