package creative.com.cryptowallet.cryptocurrencies

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.view.View
import android.widget.ProgressBar
import android.widget.TextView
import creative.com.cryptowallet.R
import creative.com.cryptowallet.model.CoinItem
import creative.com.cryptowallet.portfolio.UserPortfolioActivity
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_crypto_currencies.*
import javax.inject.Inject


/**
 * CryptoCurrenciesActivity
 * This activity is responsible for showing the user a list of crytpo coins
 */
class CryptoCurrenciesActivity : AppCompatActivity(), CryptoCurrenciesView,
        SwipeRefreshLayout.OnRefreshListener  {


    @Inject
    lateinit var mCryptoCurrenciesPresenter: CryptoCurrenciesPresenter

    private val mProgressBar: ProgressBar by lazy {
        findViewById<ProgressBar>(R.id.progress_bar)
    }
    private val mStatusTextView: TextView by lazy {
        findViewById<TextView>(R.id.status_text_view)
    }
    private val mRecyclerView: RecyclerView by lazy {
        findViewById<RecyclerView>(R.id.recycler_view) as RecyclerView
    }
    private val mSwipeRefreshLayout: SwipeRefreshLayout by lazy {
        findViewById<SwipeRefreshLayout>(R.id.swipe_layout) as SwipeRefreshLayout
    }
    private val toolbar: Toolbar by lazy {
        findViewById<Toolbar>(R.id.toolbar) as Toolbar
    }

    private val mCryptoCurrenciesAdapter = CryptoCurrenciesAdpater()

    private val layoutManager = LinearLayoutManager(this)

    private val lastVisibleItemPosition: Int
        get() = layoutManager.findLastVisibleItemPosition()

    private var onRefresh: Boolean = false


    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_crypto_currencies)
        setSupportActionBar(toolbar)
        prepareSwipeRefreshLayout()
        prepareRecyclerView()
        mRecyclerView.adapter = mCryptoCurrenciesAdapter
        mCryptoCurrenciesPresenter.onViewCreated(this, savedInstanceState)
        setRecyclerViewScrollListener()

        fab.setOnClickListener { _ ->
            val intent = Intent(this, UserPortfolioActivity::class.java)
            startActivity(intent)
        }

    }



    private fun prepareSwipeRefreshLayout() {
        mSwipeRefreshLayout.setColorSchemeColors(Color.RED, Color.GREEN, Color.BLUE, Color.YELLOW)
        mSwipeRefreshLayout.setOnRefreshListener(this)
    }

    private fun prepareRecyclerView() {
        mRecyclerView.layoutManager = layoutManager
    }

    override fun onCoinsItemLoaded(coinsItems: List<CoinItem>) {
        mSwipeRefreshLayout.isRefreshing = false
        mProgressBar.visibility = View.GONE
        if(coinsItems.isEmpty()) {
            mStatusTextView.setText(R.string.no_internet_connection)
            return
        }
        mStatusTextView.text = null

        if (onRefresh) {
            onRefresh = false
            mCryptoCurrenciesAdapter.setDataSource(coinsItems, false)
        } else {
            mCryptoCurrenciesAdapter.setDataSource(coinsItems, true)
        }

    }

    override fun onError() {
        mSwipeRefreshLayout.isRefreshing = false
        mProgressBar.visibility = View.GONE
        mStatusTextView.setText(R.string.unkown_error)
        onRefresh = false
    }

    private fun setRecyclerViewScrollListener() {
        mRecyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView?, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                val totalItemCount = recyclerView!!.layoutManager.itemCount
                if (!mCryptoCurrenciesPresenter.areCoinsLoading() &&
                        totalItemCount == lastVisibleItemPosition + 1) {
                    mCryptoCurrenciesPresenter.loadCoins()
                }
            }
        })
    }

    override fun hideLoading() {
        mSwipeRefreshLayout.isRefreshing = false
        mProgressBar.visibility = View.GONE
    }

    override fun showLoading() {
        mProgressBar.visibility = View.VISIBLE
    }

    override fun onRefresh() {
        this.onRefresh = true
       mCryptoCurrenciesPresenter.refreshCoins()
    }

    override fun onStop() {
        super.onStop()
        mCryptoCurrenciesPresenter.onDestroy()
    }



}
