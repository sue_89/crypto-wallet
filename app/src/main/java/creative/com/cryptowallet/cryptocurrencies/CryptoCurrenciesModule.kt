package creative.com.cryptowallet.cryptocurrencies

import creative.com.cryptowallet.commons.AppSchedulerProvider
import creative.com.cryptowallet.commons.CryptoApi
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by ponssusa on 04/07/2018.
 */
@Module
 class CryptoCurrenciesModule {

 @Provides
 fun provideCryptoCurrenciesPresenter(appScheduler: AppSchedulerProvider, cryptoApi: CryptoApi): CryptoCurrenciesPresenter {
   return CryptoCurrenciesPresenter(appScheduler, cryptoApi)
 }


}