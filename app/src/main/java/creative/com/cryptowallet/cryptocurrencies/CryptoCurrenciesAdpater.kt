package creative.com.cryptowallet.cryptocurrencies

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import creative.com.cryptowallet.R
import creative.com.cryptowallet.coindetails.CoinDetailsActivity
import creative.com.cryptowallet.commons.Constants
import creative.com.cryptowallet.model.CoinItem

/**
 * CryptoCurrenciesAdpater
 * This class is responsible for handleling the RecycleView Adapter.
 * It handles the different icons data and display thems
 */
class CryptoCurrenciesAdpater : RecyclerView.Adapter<CryptoCurrenciesAdpater.CryptoCurrenciesViewHolder>() {

    private var mDataSource: ArrayList<CoinItem> =ArrayList()

    /**
     * Add new coinst to the data source
     * @param dataSource icons to add to the Adpater
     * @param concatenate true, if icons should be added. False, if they should be overwritten
     */
    fun setDataSource(dataSource: List<CoinItem>, concatenate: Boolean) {
        if (!concatenate)
            this.mDataSource.clear()

        this.mDataSource.addAll(dataSource)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CryptoCurrenciesViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.view_coins_item, parent, false)

        return CryptoCurrenciesViewHolder(view)
    }



    override fun onBindViewHolder(holder: CryptoCurrenciesViewHolder, position: Int) {
        val newsItem = mDataSource[position]
        holder.bind(newsItem)

    }


    override fun getItemCount(): Int {
        return mDataSource.size
    }

    /**
     * CryptoCurrenciesViewHolder
     * This class handle each item of the list individually
     */
    class CryptoCurrenciesViewHolder internal constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var coinItem: CoinItem? = null

        private val mName: TextView by lazy {
            itemView.findViewById<TextView>(R.id.coinName)
        }

        private val mCoinSymbol: TextView by lazy {
            itemView.findViewById<TextView>(R.id.coinSymbol)
        }

        private val mPriceUsd: TextView by lazy {
            itemView.findViewById<TextView>(R.id.priceUsd)
        }

        private val mHour: TextView by lazy {
            itemView.findViewById<TextView>(R.id.oneHour)
        }

        private val mWeek: TextView by lazy {
            itemView.findViewById<TextView>(R.id.sevenDay)
        }


        fun bind(coinItem: CoinItem) {
            this.coinItem = coinItem
            mName.text = coinItem.name
            mPriceUsd.text = coinItem.priceUsd
            mHour.text = coinItem.changeHour
            mWeek.text = coinItem.changeWeek
            mCoinSymbol.text = coinItem.symbol

            // Listes for Click events
            itemView.setOnClickListener {
                val intent = Intent(itemView.context, CoinDetailsActivity::class.java)
                intent.putExtra(Constants.ICON_ITEM_KEY, coinItem.id)
                itemView.context.startActivity(intent)
            }

        }

    }

}