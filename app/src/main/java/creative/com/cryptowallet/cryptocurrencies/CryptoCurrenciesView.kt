package creative.com.cryptowallet.cryptocurrencies

import creative.com.cryptowallet.model.CoinItem

/**
 * This class is the View Interface for the CryptoCurrenciesActivity.
 * It provides acces to the Activity from the presenter
 */
interface CryptoCurrenciesView {

    /**
     * A new Currency item has been loaded
     * @param coinsItems A list of all the coins loaded
     */
    fun onCoinsItemLoaded(coinsItems: List<CoinItem>)

    /**
     * An error has happened
     * @throws throwable
     */
    fun onError()

    /**
     * Hide the loading ui
     */
    fun hideLoading()

    /**
     * Show the loading ui
     */
    fun showLoading()

}