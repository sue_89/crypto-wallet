package creative.com.cryptowallet

import creative.com.cryptowallet.commons.CryptoApi
import creative.com.cryptowallet.cryptocurrencies.CryptoCurrenciesPresenter
import creative.com.cryptowallet.cryptocurrencies.CryptoCurrenciesView
import creative.com.cryptowallet.model.CoinItem
import creative.com.cryptowallet.model.CoinsResponse
import creative.com.cryptowallet.util.TestSchedulerProvider
import io.realm.Realm
import io.realm.RealmQuery
import io.realm.RealmResults
import io.realm.log.RealmLog
import org.junit.Test

import org.junit.Before
import org.junit.Rule
import org.mockito.BDDMockito.then
import org.mockito.Mock
import org.mockito.junit.MockitoJUnit
import io.realm.internal.RealmCore
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers.anyInt
import org.powermock.api.mockito.PowerMockito.*
import org.powermock.core.classloader.annotations.PrepareForTest
import org.powermock.modules.junit4.PowerMockRunner
import rx.Observable
import org.mockito.Mockito






/**
 * CryptoCurrenciesPresenterUnitTest
 * This class tests the functionalites for the CryptoCurrencies presenter
 **/
@RunWith(PowerMockRunner::class)
@PrepareForTest(Realm::class, RealmCore::class, RealmLog::class, RealmQuery::class)
class CryptoCurrenciesPresenterUnitTest {

    @JvmField @Rule
    var mockitoRule = MockitoJUnit.rule()

    lateinit internal var mockRealm: Realm

    // mock profile View
    @Mock private lateinit var mockView: CryptoCurrenciesView

    // mock profile View
    @Mock private lateinit var cryptoApi: CryptoApi

    private lateinit var presenter: CryptoCurrenciesPresenter

    @Before
    fun setUp() {
        // Setup Realm to be mocked. The order of these matters
        mockStatic(RealmCore::class.java)
        mockStatic(RealmLog::class.java)
        mockStatic(Realm::class.java)
        val realQueryMock : RealmQuery<CoinItem> = mock(RealmQuery::class.java) as RealmQuery<CoinItem>

        // Create the mock
        mockRealm = mock(Realm::class.java)

        // Anytime getInstance is called with any configuration, then return the mockRealm
        `when`(Realm.getDefaultInstance()).thenReturn(mockRealm)
        `when`(mockRealm.where(CoinItem::class.java)).thenReturn(realQueryMock)
        //`when`(mockRealm.where(CoinItem::class.java).findAll()).thenReturn(realmResultMock)

        `when`(cryptoApi.getCoins(anyInt())).thenReturn(Observable.just(CoinsResponse()))

        presenter = CryptoCurrenciesPresenter(TestSchedulerProvider())
    }



    @Test
    fun onViewCreatedFirstTimeLoadingIsShown() {

        // Given
        presenter.setNewsApiInterface(cryptoApi)


        // when
        presenter.onViewCreated(mockView)

        //then
        then(mockView).should().showLoading()
    }

    @Test
    fun onViewCreatedFirstTimeCallGetCoins() {
        // Given
        presenter.setNewsApiInterface(cryptoApi)

        // when
        presenter.onViewCreated(mockView)

        // Then
        then(cryptoApi).should().getCoins(1)

    }

}
