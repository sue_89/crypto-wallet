package creative.com.cryptowallet.util

import creative.com.cryptowallet.commons.SchedulerProvider
import rx.Scheduler
import rx.schedulers.Schedulers

/**
 * TestSchedulerProvider
 */
class TestSchedulerProvider : SchedulerProvider {
    override fun ui(): Scheduler {
        return Schedulers.trampoline()
    }

    override fun computation(): Scheduler {
        return Schedulers.trampoline()
    }

    override fun trampoline(): Scheduler {
        return Schedulers.trampoline()
    }

    override fun newThread(): Scheduler {
        return Schedulers.trampoline()
    }

    override fun io(): Scheduler {
        return Schedulers.trampoline()
    }
}